import java.util.*;

class Main{
    public static void main(String []args){
        Scanner in;
        int count;
        int i;
        int steps;
        Integer item;
        Integer[] items;
        BinTree<Integer> tree;
        Comparator cmp;

        in=new Scanner(System.in);
        tree=new BinTree<Integer>();
        cmp=new CompInt();
        
        System.out.println("Number of items ");
        count=Integer.parseInt(in.nextLine());
        items=new Integer[8*count];
        tree.init(items);
        for(i=0;i<count;i++){
            System.out.println("Item to insert ");
            item=Integer.parseInt(in.nextLine());
            tree.insert(items,item,cmp);
        }
        System.out.println("Item to find ");
        item=Integer.parseInt(in.nextLine());
        steps=tree.found(items,item,cmp);
        System.out.printf("Found %d in %d steps\n",item,steps);
    }
}
/*
*
* EXAMPLE OUTPUT
*
* Found 9 in 3 steps
*
* COMPILE INSTRUCTION
*
* javac Main.java
*
*/
