import java.util.*;

class MainTest{
    public static void main(String []args){
        Scanner in;
        int count;
        int i;
        int steps;
        Integer item;
        Integer[] items;
        BinTree<Integer> tree;
        Comparator cmp;

        in=new Scanner(System.in);
        tree=new BinTree<Integer>();
        cmp=new CompInt();
        
        count=Integer.parseInt(in.nextLine());
        items=new Integer[8*count];
        tree.init(items);
        for(i=0;i<count;i++){
            item=Integer.parseInt(in.nextLine());
            tree.insert(items,item,cmp);
        }
        item=Integer.parseInt(in.nextLine());
        steps=tree.found(items,item,cmp);
        System.out.printf("Found %d in %d steps\n",item,steps);
    }
}
/*
*
* EXAMPLE OUTPUT
*
* Found 9 in 3 steps
*
*/
