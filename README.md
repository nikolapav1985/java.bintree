Binary tree array based implementation
--------------------------------------

- generic implementation of a binary tree (any type supported)
- array based
- root index 1
- right child 2r+2
- left child 2r+1
