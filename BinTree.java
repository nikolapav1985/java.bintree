import java.util.*;

class BinTree<T>{ // array based binary tree
    protected int count;
    public BinTree(){
        this.count=0; // number of elements in a tree
    }
    public void init(T[] tree){ // initialize a tree
        int i=0;
        for(;i<tree.length;i++){
            tree[i]=null;
        }
    }
    public void insert(T[] tree, T item, Comparator cmp){ // insert item into binary tree
        int i=1,cmpr;
        if(this.count<=0){ // tree empty
            tree[1]=item;
            this.count++;
            return;
        }
        for(;true;){
            cmpr=cmp.compare(item,tree[i]);
            if(cmpr==1){
                i=2*i+2;
            }else if(cmpr==-1){
                i=2*i+1;
            }else{ // equal values, nothing to insert
                break;
            }
            if(tree[i]==null){
                tree[i]=item;
                this.count++;
                break;
            }
        }
    }
    public int found(T[] tree, T item, Comparator cmp){ // find item in binary tree
        int i=1,cmpr,count=0;
        if(this.count<=0){ // tree empty
            return -1;
        }
        for(;i<tree.length;count++){
            cmpr=cmp.compare(item, tree[i]);
            if(cmpr==1){
                i=2*i+2;
            }else if(cmpr==-1){
                i=2*i+1;
            }else{ // equal values, item found
                break;
            }
        }
        return count;
    }
}

